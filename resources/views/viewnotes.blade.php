<!DOCTYPE html>
<html>
	<head>
		<title> MINI APP</title>
		<style type="text/css">
			td {
				max-width: 400px;
				padding-right: 10px;
				padding-left: 10px; 
				padding-top: 5px;
				padding-bottom: 5px;
			}

			thead tr td{
				border-bottom: 2px solid;
				font-size: 18px;
				font-weight: bold;
			}

			body {
	                margin: 0;
	                padding: 0;
	                width: 100%;
	                display: table;
	                font-family: 'Lato';
	            }

			img {
		            	margin: 10px;
		            }

		     table{
		     	min-width: 650px;
		     }
		</style>
	</head>
	<header>
		<div>
			<div style="float: left; padding: 10px;">
				<a href="{{ url('index') }}" name="back"><img src="{{ asset('imgs/flecha-icon.png') }}"></a><br>
			</div>
		    <div style="float: right; padding: 10px;">
		        <a href="{{ url('newnote') }}" name="addnote"><img src="{{ asset('imgs/plus-icon.png') }}"></a><br>  
		    </div>
		</div>
	</header>
	<body>		
		<div style="padding-top: 200px;">


		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif


			<table align="center" cellspacing="0">
				<thead>
					<tr>
						<td>
							Title	
						</td>
						<td>
							Date to do
						</td>
						<td>
							Description
						</td>
						<td>
							Status
						</td>
						<td>
							Actions
						</td>
					</tr>
				</thead>
				<tbody>
					@foreach($notes as $note)
					<tr>
						<td>
							{{$note->title}}
						</td>
						<td>
							{{$note->date}}
						</td>
						<td>
							{{$note->description}}
						</td>
						<td>
							{{$note->status}}
						</td>
						<td align="center">

							
							<form action="{{url('/viewnotes/'.$note->id.'/edit')}} ">
								<button style="width: 50px; background-color: #80FFDC; border: none; border-radius: 5px;">edit</button>
							</form>

							<form action="viewnotes/{{ $note->id }}" method="POST">
								{{ csrf_field() }}
					            {{ method_field('DELETE') }}
								<button style="width: 50px; background-color: #FF9C82; border: none; border-radius: 5px;" onclick="return confirm('Are you sure you want to delete this note?');">delete</button>
							</form>

						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</body>
</html>