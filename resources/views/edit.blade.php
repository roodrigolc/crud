<!DOCTYPE html>
<html>
<head>
	<title> MINI APP</title>
	<style type="text/css">
		body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-family: 'Lato';
            }

            img {
            	margin: 10px;
            }

            body div {
            	padding-top: 200px;
            	display: block;
		    	margin: auto;
		    }

		    header div {
		    	padding:0;
		    }

		    body div form table{
		    	margin: auto;
		    }

		    .button {
		    	color: black;
		    	padding-top: 1px;
		    	padding-bottom: 1px;
		    	padding-right: 5px;
		    	padding-left: 5px;
		    	font-size: 12px;
		   		text-decoration: none; 
		    	background-color: #eeeeee; 
		    	border: solid;
		    	border-width: 1px;
		    	border-color: #dddddd; 
		    	border-radius: 4px; 
		    	margin-right: 10px;
		    }

		    .button:active {
		    	background-color: #dddddd;
		    }

		    .alert-danger {
		     	margin-top: 50px;
		     	left: 0;
		     	right: 0;
		     	padding-top: 10px;
		     	padding-bottom: 10px;
		    	background-color: #eeeeee;
		    }

		    .alert-danger p { 
		    	color: gray;
    			margin-left: 50px;
}

	</style>
</head>
<header>
	<div>
		<div style="float: left; padding: 10px;">
			<a href="{{ url('viewnotes') }}" name="back"><img src="{{ asset('imgs/flecha-icon.png') }}"></a><br>
		</div>
	    <div style="float: right; padding: 10px;">
	        <a href="{{ url('newnote') }}" name="addnote"><img src="{{ asset('imgs/plus-icon.png') }}"></a><br>
	        <a href="{{ url('viewnotes' )}}"><img src="{{ asset('imgs/view-icon.png') }}"></a> 
	    </div>
	</div>
</header>
<body>
	<div style="padding-top: 200px;">
		<form method="POST" action="{{url('/viewnotes/'.$note->id)}}">
			{{ method_field('PUT') }}
			<table>
				<tr>
					<td>
						 <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
						<label>Note title @if (count($errors) > 0)<span style="color: red;">*</span>@endif </label>
					</td>
					<td>
						<input value="{{ $note->title }}" type="text" name="title" maxlength="30" id="title">
					</td>
				</tr>
				<tr>
					<td>
						<label>Date to accomplish @if (count($errors) > 0)<span style="color: red;">*</span>@endif </label>
					</td>
					<td>
						<input value="{{ $note->date }}" type="date" name="date" max="2050-12-31" min="2017-01-01" id="date">
					</td>
				
				<tr>
					<td>
						<label>Description</label>
					</td>
					<td>
						<input value="{{ $note->description }}" type="text" name="description" maxlength="300" style="width: 300px; height: 100px; overflow: visible; resize: none;" id="description"></input>
					</td>
				</tr>
				<tr>
					<td>
						<label>Status</label>
					</td>
					<td>
						<select name="status" id="status">
						  <option value="hanging">Hanging :(</option>
						  <option value="done">Done! :)</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<input class="button" type="reset" name=""">
						<a class="button" href="{{ url('/viewnotes') }}">Cancel</a>
						<input class="button" type="submit" name="add-note" value="Finish" onclick="confirm('Are you done?');">	
					</td>
				</tr>
			</table>
		</form>
		<table>
			<tr>
				<td></td>
			</tr>
		</table>
		@if (count($errors) > 0)
    	<div class="alert alert-danger">
			<p>The fields marked with a red 
asterisk are mandatory.</p>        
    	</div>
	@endif
	</div>
	
</body>
</html>