<!DOCTYPE html>
<html>
    <head>
        <title> MINI APP</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-family: 'Lato';
            }

            .container {
                padding-top: 200px;
                text-align: center;
            }

            .title {
                margin: auto;
                display: block;
                font-size: 96px;
            }
            .minititle {
                font-size: 30px;
            }

            header{ 
                width: 100%;
                height: 50px;

            }

            img {
                margin: 10px;
            }
        </style>
    </head>
    <header>
        <div style="float: right; padding: 10px;">
            <a href="{{ url('newnote') }}" name="addnote"><img src="{{ asset('imgs/plus-icon.png') }}"></a><br>
            <a href="{{ url('viewnotes' )}}"><img src="{{ asset('imgs/view-icon.png') }}"></a> 
        </div>
    </header>
    <body>
        <div class="container">
            <div class="title">HELLO</div>
            <div class="minititle" >make a note!</div>
        </div>
    </body>
</html>
