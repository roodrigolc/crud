<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Note;

Route::get('/', function () {
    return redirect('/index');
});

Route::get('/index', function () {
    return view('welcome');
});

Route::get("newnote", "NotesControler@index");
Route::post("store", "NotesControler@store");


Route::get('editnotes', function () {
    return view('editnote');
});

Route::get('viewnotes', 'NotesControler@viewnotes');

Route::get('newnote', function () {
    return view('newnote');
});

Route::delete('/viewnotes/{note}', 'NotesControler@destroy');
 

Route::get('editnote', function () {
    return view('edit');
});


Route::get('/viewnotes/{id}/edit','NotesControler@edit');

Route::put('/viewnotes/{id}', 'NotesControler@update');

